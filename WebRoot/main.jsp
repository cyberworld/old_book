<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String logname = (String) session.getAttribute("logname");
	if (logname == null || logname.equals("")) {
		out
				.print("<script>alert('你还没有登录，请先登录！');self.location.replace('myweb.jsp');</script>");
		return;
	}
%>

<html lang="en">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Document</title>
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.css">
		<style>
.navbar {
	background-color: rgb(34, 92, 168); wdith：100px;
	padding: 10px;
	height: 80px;
	font-size: 40px;
	color: white;
	wdith: 1000px;
}

.navbar p {
	font-family: 微软雅黑;
	text-align: center;
	background-color: 
}
</style>
	</head>

	<body>


		<div class="container">

			<div class="navbar">

				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#top-menu">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<p>
					预定留言板
				</p>

			</div>
		</div>
		<form action="LeaveMessageServlet" class="form-horizontal">
			<input type="hidden" name="action" value="new">
			<div class="form-group">
				<label for="" class="col-md-4 control-label">
					用户号
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="userid" id="userid">
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-md-4 control-label">
					姓名
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="name" id="name">
				</div>
			</div>


			<div class="form-group">
				<label for="" class="col-md-4 control-label">
					地址
				</label>
				<div class="col-md-4">
					<select class="form-control" id="address">
						<option>
							23#
						</option>
						<option>
							24#
						</option>
						<option>
							25#
						</option>
						<option>
							26#
						</option>
						<option>
							16#
						</option>
						<option>
							17#
						</option>
						<option>
							18#
						</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-4 control-label">
					电话
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="phone" id="phone">
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-4 control-label">
					学生所在系
				</label>
				<div class="col-md-4">
					<select class="form-control" id="dep">
						<option>
							计算机
						</option>
						<option>
							工程
						</option>
						<option>
							会计
						</option>
						<option>
							国际教育
						</option>
						<option>
							数学
						</option>
						<option>
							经济
						</option>
						<option>
							金融
						</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-4 control-label">
					书名
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="bookname"
						id="bookname">
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-md-4 control-label">
					出版社
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="press" id="press">
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-md-4 control-label">
					数量
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="number" id="number">
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-md-4 control-label">
					日期
				</label>
				<div class="col-md-4">
					<input class="form-control" type="text" name="day" id="day">
				</div>
			</div>
			<p align="center">
				<button type="submit" class="btn btn-primary">
					添加
				</button>
				<button type="reset" class="btn btn-default">
					重置
				</button>
			</p>
			<a href="myweb.jsp">&lt;&lt;Back </a>
		</form>		
		<script src="lib/jquery-3.2.1.min.js"></script>
		<script src="lib/bootstrap/js/bootstrap.js"></script>
	</body>
</html>