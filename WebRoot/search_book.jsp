<%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
<jsp:useBean id="curBook" class="mybean.BookBean"></jsp:useBean>
<%
	String imgPath = request.getRealPath("/");
%>
<html lang="en">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>图书查询</title>
		<link rel="stylesheet" href="lib/bootstrap/css/bootstrap.css">
	</head>
	<style>
#header {
	padding: 10px;
	background-color: rgb(34, 92, 168);
	margin-bottom: 20px;
	margin-top: 20px;
}

#header h1 {
	font-family: 微软雅黑;
	color: #fff;
	text-align: center;
}

p {
	font-family: 微软雅黑;
	font-size: 20px;
	text-align: center;
}
#return {
	text-align: left;
}

.box {
	margin-top: 10px;
}

.item {
	
}

.item h3:hover {
	background: #33CCCC;
}

.item h3 a {
	color: #FFFFCC;
}

#sort {
	position: absolute;
	top: 200px;
	left: 200px;
}

#nav_main {
	position: absolute;
	top: 2px;
	left: 200px;
	width: 800px;
}

#nav {
	position: relative;
	height: 31.5px;
	background: url(images/nav.png) repeat center;
}

#nav_main li a {
	color: #FFFFFF;
	background: url(../images/nav.png) center;
	display: block;
	width: 100px;
	height: 27px;
	text-align: center;
	line-height: 27px;
	border-right: 1px ridge #1d53bf;
	border-left: 1px ridge #1d53bf;
}

#nav_main li a:hover {
	background: #1d53bf;
	color: #FF3333;
}

#menu {
	position: absolute;
	width: 197px;
}

#menu :hover #menu_main {
	display: block;
}

a:hover {
	color: #FF3333;
	text-decoration: underline;
}

.floor {
	clear: both;
	width: 800px;
}

.title {
font-family:幼圆;
	border: 1px solid #CCCCCC;
	height: 40px;
	font-size: 16px;
	line-height: 40px;
	color: #fff;
	background: #337AB7;
}
#yuan{
text-align: center;
}
</style>
	<script type="text/javascript">
	function MM_jumpMenu(targ, selObj, restore) {
		eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value
				+ "'");
		if (restore)
			selObj.selectedIndex = 0;
	}
</script>

	<body>

		<div class="container">

			<div class="col-md-11">
				<div id="header">
					<h1>
						图书查询
					</h1>
				</div>
				<div class="row">
					<div class="col-md-2">
						<ul class="nav nav-pills nav-stacked" id="yuan">
							<li class="active">
								<a href="#jsj">计算机院</a>
							</li>
							<li>
								<a href="#gc">工程院</a>
							</li>
							<li>
								<a href="#jr">金融院</a>
							</li>
							<li>
								<a href="#jj">经济院</a>
							</li>
							<li>
								<a href="#fx">法学院</a>
							</li>
							<li>
								<a href="#kj">会计院</a>
							</li>
							<li>
								<a href="#xd">信电院</a>
							</li>
							<li>
								<a href="#gj">国教院</a>
							</li>
							<li>
								<a href="#sx">数学院</a>
							</li>
							<li>
								<a href="#qt">其他</a>
							</li>
						</ul>
					</div>
					<div class="container">
						<div class="row">
							<div class="tab-content">

								<form class="form-horizontal" method="post"
									action="book/booktable.jsp">
									<div class="form-group">
										<label for="" class="col-md-1 control-label" id="point">
											关键字 :
										</label>
										<div class="col-md-3">
											<input class="form-control" id="key" name="key" type="text">
										</div>
										<div class="col-md-2">
											<select class="form-control" name="jumpMenu" id="jumpMenu"
												onChange="MM_jumpMenu('parent',this,0)">
												<option>
													新书名称
												</option>
											</select>
										</div>
										<div class="col-md-1">
											<input type="submit" name="button" id="button" value="搜索">
										</div>
										<div class="col-md-2">
											<button>
												<a class="point" href="myweb.jsp">返回首页</a>
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div id="sort">
						<div class="floor">
							<div class="title">
								<h4 id="jsj">
									计算机
								</h4>
							</div>
							<div id="curbook" class="box">
								<%=curBook.getSortjsj(imgPath)%>
							</div>
						</div>

						<div class="floor">
							<div class="title">
								<h4 id="gc">
									工程院
								</h4>
							</div>
							<div id="curbook" class="box">
								<%=curBook.getSortgl(imgPath)%>
							</div>
						</div>

						<div class="floor">
							<div class="title">
								<h4 id="jr">
									金融院
								</h4>
							</div>
							<div id="curbook" class="box">
								<%=curBook.getSortjr(imgPath)%>
							</div>
						</div>

						<div class="floor">
							<div class="title">
								<h4 id="jj">
									经济院
								</h4>
							</div>
							<div id="curbook" class="box">
								<%=curBook.getSortjj(imgPath)%>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="lib/jquery-3.2.1.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.js"></script>
	</body>

</html>