<%@ page contentType="text/html; charset=GBK" language="java" import="mybean.BookBean"%>
<jsp:useBean id="book" class="mybean.BookBean"></jsp:useBean>
<%
	int bid = Integer.parseInt(request.getParameter("bid"));
	if(!book.getBook(bid))
		return;
	book.showImage(request.getRealPath("/"),bid);
%>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>详细资料</title>
    <script  type="text/javascript">
function openwin(bid) { 
	var url = "../orders/orderForm.jsp?bid="+ bid;
	var title = "确认订单";
	window.open (url, title, "height=300, width=400, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
} 
</script>
    <link rel="stylesheet"  href="../lib/bootstrap/css/bootstrap.css">
</head>
<style>
    .header {
        padding: 10px;
        background-color: rgb(34, 92, 168);
        margin-bottom: 20px;
        margin-top: 20px;

    }

    .header h1 {
        font-family: 微软雅黑;
        color: #fff;
        text-align: center;
    }

    p {
        font-family: 微软雅黑;
        font-size: 20px;
        text-align: center;
    }
    #return{
        text-align: left;

    }
</style>
<body>

    <div class="container" >
        <div class="header">
            <h1>书的详细资料</h1>
        </div>
        <div class="">
            
             
                    <table class="table table-striped table-bordered">
                       <tr>
                           <th class="col-md-2">书名称：</th>
                           <th><%=book.getBookName() %></th>
                       </tr>
                       <tr>
                        <td>类型：</td>
                        <td><%=book.getBookType(2)%></td>
                    </tr>
                    <tr>
                        <td>出版社:</td>
                        <td><%=book.getPress() %></td>
                    </tr>
                    <tr>
                        <td>单本价格：</td>
                        <td><%=book.getPrice() %></td>
                    </tr>
                    <tr>
                        <td>库存：</td>
                        <td><%=book.getRepertory() %></td>
                    </tr>
                    <tr>
                        <td>上货时间:</td>
                        <td><%=book.getCommend()%></td>
                    </tr>
                    <tr>
                        <td>书的描述：</td>
                        <td><%=book.getIntro() %></td>
                    </tr>
                    <tr>
                        <td>图片：</td>
                        <td valign="bottom"><label><img src="../images/<%=book.getBookId()%>.jpg" width="80" height="120" alt=""></label></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><a href=# onclick="openwin('<%=book.getBookId()%>');">放入购物车</a></td>
                    </tr>
                
                      </table>
                   
                </div>
          
        <div id="return">
	        <a href="javascript:history.back(-1)">返回上一页</a>&nbsp;&nbsp;&nbsp;
	        <a href="../myweb.jsp">返回首页</a>
        </div>
    </div>
    
    <script src="../lib/jquery-3.2.1.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
</body>
</html>