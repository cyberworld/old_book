<%@ page contentType="text/html; charset=GBK" language="java" import="mybean.category" %>
<jsp:useBean id="typeList" class="mybean.category"></jsp:useBean>
<html>
<head>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>新书上架</title>
    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.css">
</head>
<style>
    .header {
        padding: 10px;
        background-color: rgb(34, 92, 168);
        margin-bottom: 20px;
        margin-top: 20px;

    }

    .header h1 {
        font-family: 微软雅黑;
        color: #fff;
        text-align: center;
    }

    #return {
        text-align: left;

    }

    #lab{
     text-align: center;
    }
</style>
<script  type="text/javascript">

	var xmlHttp;
	function createXMLReq( ){
		if(window.XMLHttpRequest){ 
			xmlHttp = new XMLHttpRequest(); 
	     } else if(window.ActiveXObject){ 
	    	xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); 
	     }
	}

  	function sendPostReq(url, param){
  	  	createXMLReq();   	   	  	
  	  	xmlHttp.open("POST", url, true);
  	  	xmlHttp.setRequestHeader("Content-Type",
  	    	  	"application/x-www-form-urlencoded; charset=UTF-8");
  	  xmlHttp.onreadystatechange = callback;
	  	xmlHttp.send(param);
  	}

  	function callback(){
  	  	if(xmlHttp.readyState == 4){
  	  	  	if(xmlHttp.status == 200){
  	    	  	dowhat();
  	  	  	}else{
  	  	  	  	window.alert("page error! " + xmlHttp.status);
  	  	  	}
  	  	}
  	}

  	function dowhat(){
  	  	if(xmlHttp.responseText.indexOf("2")!=-1)
  	  	  	alert("添加成功");
  	  	else alert("添加失败");
  	}
     var param="";
  	function req(){
  	  	var obj = document.getElementById("addbook");
  	  	var bn = obj.bookName.value;
  	  	var bt = obj.bookType.value;
  	  	var ps = obj.press.value;
  	  	var pc = obj.price.value;
  	  	var rp = obj.repertory.value;
  	  	var cm = obj.Commend.value;
  	  	var tm = obj.times.value;
  	  	var tr = obj.intro.value;

  	  	var url = "../bookAction?act=add";
  	   param = "bookName="+bn+"&bookType="+bt+"&press="+ps+"&price"+pc
  	  				+"&repertory="+rp+"&Commend="+cm+"&times="+tm+"&intro="+tr;
  	  	sendPostReq(url, param);
  	}

  	function chkBN( ){
  	  	var obj = document.getElementById("bookName");
  	  	var v = obj.value;
  	  	if(v == "")
  	  	  	alert("新书名称不能为空！");
  	}

  	function setDate(){
  		var obj = document.getElementById("times");
  		var now = new Date();
  		var year= now.getFullYear();
  		var month=now.getMonth()+1;
  		var day=now.getDay();
  		obj.value = year.toString()+"-"+month.toString()+"-"+day.toString();
  	}

</script>
<body onload="setDate();">

    <div class="container">
        <div class="header">
            <h1>新书上架</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="tab-content">

                    <form class="form-horizontal" id="addbook" >
                        <div id="lab">注：带*项内容必须填写！</div>
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">新书名称* :</label>
                            <div class="col-md-4">
                                <input class="form-control" name="bookName" type="text" id="bookName" onBlur="chkBN();">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">类型：</label>
                            <div class="col-md-4">
                                <select class="form-control" name="bookType">
                                    <%=typeList.getList()%>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">出版社 :</label>
                            <div class="col-md-4">
                                <input class="form-control" name="press" type="text" id="press">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">单价：</label>
                            <div class="col-md-4">
                                <input class="form-control" name="price" type="text" id="price" placeholder="请输入数字" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">库存数量 :</label>
                            <div class="col-md-4">
                                <input class="form-control" name="repertory" type="text" id="repertory" placeholder="请输入库存数量">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">上货时间：</label>
                            <div class="col-md-4">
                                <input class="form-control" name="times" type="text" id="times">

                            </div>
                            <div class="col-md-4">
                                <label>（例如：2018-01-01）</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">图书描述：</label>
                            <div class="col-md-4">
                                <textarea class="form-control" name="selfintro" id="selfintro" cols="45" rows="5"></textarea>
                            </div>
                            <div class="col-md-4">
                                <label>（一百个汉字内）</label>
                            </div>
                        </div>
                                <div class="submit" id="lab">
                                      <label>
            <input type="submit" name="button" id="button" value="提交" onClick="req();">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="reset" name="button2" id="button2" value="重设">
          </label>
                                </div>
                
                <div class="return">
                    <a href="../manager/manager.jsp">返回管理员首页</a>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <script src="../lib/jquery-3.2.1.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
</body>

</html>