<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK" import="mybean.BookBean"%>
<jsp:useBean id="book" class="mybean.BookBean"></jsp:useBean>
<%
	request.setCharacterEncoding("GBK");
	String keyword = request.getParameter("key");
%>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.css">
    
<meta http-equiv="Content-Type" content="text/html; charset=GBK" />
<title>搜索结果</title>
<script  type="text/javascript">
function openwin(bid) { 
	var url = "../orders/orderForm.jsp?bid="+ bid;
	var title = "确认订单";
	window.open (url, title, "height=300, width=400, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, status=no");
} 
</script>
<style type="text/css">
<!--
.STYLE3 {font-size: 16px; font-weight: bold; }
#apDiv1 {
	position:absolute;
	width:300px;
	height:200px;
	z-index:1;
	left: 465px;
	top: 140px;
	background-color: #6666FF;
	visibility: visible;
}
.STYLE4 {
	font-size: 36px;
	color: #FF0000;
}
-->
</style>
<style type="text/css">
<!--
.STYLE3 {font-size: larger}
.STYLE4 {
	font-size: xx-large;
	color: #FF0000;
	font-weight: bold;
}
.STYLE6 {color: #0099FF}
.STYLE7 {color: #0099CC}
.STYLE8 {color: #FF0000}
-->

        #tou {
            padding: 10px;
            background-color: #afd6f7;
        }

        tr {
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="container">
        <div id="od">
            <div id="header">
                <table id="tou" width="1135" border="0">
                    <tr>
                        <th height="83" scope="col">
                            <div align="center">
                                <font size="7" color="#337ab7" face="楷体">符合条件产品如下：</font>
                            </div>
                        </th>
                    </tr>
                </table>
            </div>

        </div>
        <div>
            <div width="1160">
                <table class="table  table-hover" width="1160" border="0" align="center" cellpadding="1" cellspacing="0">
                    <tr>
                        <td width="230" align="center">
                            <div align="center">
                                <span  class="STYLE3">新书名称</span>
                            </div>
                        </td>
                        <td width="238" align="center">
                            <div align="center">
                                <span  class="STYLE3">出版社</span>
                            </div>
                        </td>
                        <td width="47" align="center">
                            <div align="center">
                                <span class="STYLE3">库存</span>
                            </div>
                        </td>
                        <td width="100" align="center">
                            <div align="center">
                                <span class="STYLE3">精品推荐</span>
                            </div>
                        </td>
                        <td width="161" align="center">
                            <div align="center">
                                <span class="STYLE3">书的描述</span>
                            </div>
                        </td>
                        <td width="132" align="center">
                            <div align="center">
                                <span class="STYLE3">详细资料   </span>
                            </div>
                        </td>
                        <td width="132" align="center">
                            <div align="center">
                                <span class="STYLE3">放入购物车</span>
                            </div>
                        </td>
                        
                    
                            <%=book.getBookList(keyword) %>
	              
                </table>
            </div>
        </div>
        <div>
            <div>
                <table class="table table-striped table-border">
                    <tr>
                        <th height="83" scope="col">
                            <div align="center">
                            <p>&nbsp;</p>
                                <span class="STYLE3">
                                        <p align="center"><a href="../myweb.jsp">返回首页</a>&nbsp;
                                            <a href="../search_book.jsp">返回搜索</a>&nbsp;
                                            <a href="../orders/orderlst.jsp">查看购物车</a></p>
                                </span>
                            </div>
                        </th>
                    </tr>
                </table>
            </div>

        </div>
    </div>
    <script src="../lib/jquery-3.2.1.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
</body>

</html>