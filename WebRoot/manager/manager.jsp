<%@ page language="java" contentType="text/html; charset=utf-8"
    import="mybean.BookBean"%>
    <jsp:useBean id="book" class="mybean.BookBean"></jsp:useBean>
<%
	String action = request.getParameter("act");
	int act = 1;
	if(action != null)
		act = Integer.parseInt(action);
%>
<html >

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.css">
    <style>
.navbar{
   background-color:rgb(34, 92, 168);
    wdith：100px;
   padding:10px;
    height:80px;
    font-size:40px;

    color:white;
}
  .navbar p{
  font-family:微软雅黑;
  text-align:center;
  }      
    </style>
</head>

<body>


        <div class="container">

            <div class="navbar">
       
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-menu">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <p>管理员界面</p>
                   
                </div>
            </div>


    <div class="container">

        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-pills nav-stacked">
                    <li>
                        <a href="userlst.jsp">用户管理</a>
                    </li>
                    <li>
                        <a href="../book/addbook.jsp">新书上架</a>
                    </li>
                    <li>
                        <a href="../getmain.jsp">查看留言</a>
                    </li>
                    <li>
                        <a href="../orders/list.jsp">订单管理</a>
                    </li>
                     <li>
                        <a href="<%=request.getContextPath()%>/logaction">退出</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">

                <ul class="my-tab nav nav-tabs">
                    <li>
                        <a href="manager.jsp?act=1">未添加图片</a>
                    </li>
                    <li>
                        <a href="manager.jsp?act=2">已添加图片</a>
                    </li>
                    <li>
                            <a href="manager.jsp?act=0">全部</a>
                        </li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane active" id="student">
                       
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>书名</th>
                                    <th>出版社</th>
                                    <th>精品推荐</th>
                                    <th>操作</th>
                                </tr>
                                
                            </thead>
                            <tbody>
                             <%=book.getBookList(act) %>  
                                
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="tab-pane" id="class">class</div>
                </div>
            </div>
        </div>

    </div>

    
    <script src="../lib/jquery-3.2.1.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
</body>

</html>