<%@ page contentType="text/html; charset=GBK" language="java" import="mybean.BookBean"%>
<jsp:useBean id="book" class="mybean.BookBean"></jsp:useBean>
<%
	int bid = Integer.parseInt(request.getParameter("bid"));
	if(!book.getBook(bid))
		return;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK" />
<title>图片上传</title>
</head>

<body>
  <a href="manager.jsp">【管理员首页】
  </a>

<h1 align="center">书的详细资料</h1>
  <table width="698" border="1" align="center">
    <tr>
      <td width="221" align="right">书名称：</td>
      <td width="461"><%=book.getBookName() %></td>
    </tr>
    <tr>
      <td align="right">类型：</td><td><%=book.getBookType(2)%></td>
    </tr>
    <tr>
      <td align="right">出版社：</td> <td><%=book.getPress() %></td>
    </tr>
    <tr>
      <td align="right">单本价格：</td><td><%=book.getPrice() %></td>
    </tr>
    <tr>
      <td align="right">库存：</td><td><%=book.getRepertory() %></td>
    </tr>
    <tr>
      <td align="right">精品推荐：</td><td><%=book.getCommend()%></td>
    </tr>
    <tr>
      <td align="right">上货时间：</td><td><%=book.getTimes() %></td>
    </tr>
    <tr>
      <td align="right">书的描述：</td><td><%=book.getIntro() %></td>
    </tr>
    <tr>
      <td height="80" align="center"><img src="" width="80" height="120" alt=""><br>
      书暂无图片，请上传 </td>
      <td valign="bottom">
	<FORM  METHOD="POST" ACTION="../ImgUpload?bid=<%=bid %>" ENCTYPE="multipart/form-data">
        <input type="file" name="filename" id="filename">
		<input type="submit" value="上传" />
      </form>      <label></label></td>
    </tr>
  </table>
<br />
<br />
<a href="javascript:self.close()">【关闭窗口】</a>
<p>&nbsp;</p>
</body>
</html>