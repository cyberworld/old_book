<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<% String rand=(String)session.getAttribute("rand");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    
    <title>用户登录</title>
   
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link rel="stylesheet"	type="text/css"	href="css/foot.css"	/>
	<link href="css/search.css" rel="stylesheet" type="text/css">
	
	<script type="text/javascript" src="js/jquery-1.7.2.js"></script>
	<script type="text/javascript" src="js/script.js" charset="gbk"></script>
	
	<style type="text/css">
		#top{
			width:100%;
			line-height:35px;
			background-color:#F6F6F6;
			height:35px;
			font-size:14px;
			color:black;
		}
		
		#top #left1{
			width:350px;
			float:left;
			color:black;
		}
		#top #right1{
			float:right;
			text-align:right;
			color:black;
		}
		#top li{
			float:left;
			height:27px;
			padding:0 10px;
		}
		.submit{border:none; font-weight:bold;color:#FFF; margin-top:21px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow: #CCC 0px 0px 5px;-moz-box-shadow: #CCC 0px 0px 5px;box-shadow: #CCC 0px 0px 5px;background: #06B; cursor: pointer;}
		.submit:hover{ background:#ff9229;}
		.submit{ padding:6px 20px;width:70px;height:30px;}
		input{
			height:25px;
			width:150px;
			font-size:16px;
		}
		label{
			font-size:"幼圆";
			display:inline-block;
			width:55px;
			font-size:16px;
			padding-right:10px;
			vertical-align:middle;
		}
		
		#header{
			padding:19px 0px 0px 0px;
			height:auto;
			vertical-align:middle;
		}
		.left2{
		margin-left:30px;
			float:left;
		}
		#header a{
			margin-right:20px;
		}
		#login {
			background: transparent url("images/login.jpg") left top no-repeat;
			height: 460px;
			margin-left:50px;
			margin-top: 65px;
			padding-top: 20px;
		}
		div{
			display:block;
		}
		
		
		#login #loginform {
			background: transparent url("images/00.jpg") left top repeat-y;
			height: 350px;
			margin: 0px 0px 0px 620px;
			width: 430px;
			z-index: 2;
			padding: 30px 45px 0px 45px;
			border: 2px solid #06B;
			
		}
		#login #loginform h3{
			font-size:25px;
			margin-bottom:40px;
			font-weight:bold;
			color:#06B;
		}
		#login #loginform li.inputtext {
			position:relative;
		}
		#login #loginform li.inputtext1 {
			position:relative;
		}
		#login #loginform li{
			margin-bottom:20px;
		}
		li{
			list-style:none;
		}
		body {
			font-size: 12px;
			font-family: Segoe UI,微软雅黑,Calibri,Arial,Helvetica,sans-serif,宋体;
			color: #666;
		}
		*{
			margin:0;
			padding:0;
		}
		a{
			color:#666;
			text-decoration:none;
			
		}
		a:hover{
			color:#BD0000;
			text-decoration:underline;
		}
		.w {
			width: 1200px;
			margin: 0px auto;
		}
		.inputtext input {
			background-color:#fff;
			width: 235px;
			height: 35px;
			border: 1px solid #CACACA;
			padding-left: 10px;
			line-height: 35px;
		}
		.inputtext1 input {
			background-color:#fff;
			width: 120px;
			height: 35px;
			border: 1px solid #CACACA;
			padding-left: 10px;
			line-height: 35px;
		}
		
		
		#footer {
			clear: both;
		}
		#footer {
			margin-top: 0px;
			border-top: solid 3px #06B;
		}
		#footer_top{
			padding:25px 0px 10px 60px;
		}
		#footer_top .item{
			float:left;
			width:170px;
			margin-right:15px;
		}
		#footer_top .item h3{
			font-size:22px;
			font-weight:bold;
			margin-bottom:10px;
		}
		.item li{
			font-size:15px;
			height:25px;
			line-height:25px;
			overflow:hidden;
		}
		#bottom{
			margin:50px auto 50px auto;
			text-align:center;
			clear:both;
		}
		
		#bottom h5{
			font-size:13px;
			color:black;
		}
	</style>
	
	<script type="text/javascript">
		function addFavorite(){ 
			var aUrls=document.URL.split("/"); 
			var vDomainName="";
			for(var i=0;i<aUrls.length;i++){
				vDomainName=vDomainName+aUrls[i]+"/"
			}
			var description=document.title; 
			try{//IE 
				window.external.AddFavorite(vDomainName,description); 
			}catch(e){//FF 
				window.sidebar.addPanel(description,vDomainName,""); 
			} 
		}
		
		function checkValidate(){
			var slogname=document.form1.logname.value;
			var spassword=document.form1.password.value;
			var srand=document.form1.rand.value;	
			//String srand1=srand.toString();
			//document.write(srand1);
			if(slogname==""){
				alert("用户名不能为空！！！");	
				return false;		
			}
			if(spassword==""){
				alert("密码不能为空！！！");
				return false;		
			}
			if(srand==""){
				alert("验证码不能为空！！！");
				return false;		
			}
			if(!srand.toString().equals(rand)){
				alert("验证码错误！！！");
				return false;
			}
		}
	</script>
  </head>

  <body>
  <div id="top">
  	<div class="w">
  		<ul id="left1">
  			<li>你好，欢迎来到校园二手书购物网</li>
  			<li><a href="login.jsp">[登录]</a></li>
  			<li><a href="register.jsp">[注册]</a></li>
  		</ul>
  		<ul id="right1">
  			<li></li>
  			<li></li>
  			<li><a href="" onclick="addFavorite();return false;"></a></li>
  		</ul>
  	</div>
  </div>
  
  
  <div id="frame" class="w">
  <div id="header">
  <div class="left2">
  	<a href="index.jsp"><img alt="book" src="images/logo.jpg" width="170" height="74"></a>
  </div>
  <div class="right2">
  <div id="login"> 
  <div id="loginform">
  <h3>登录</h3>
  <form action="logaction" method="post" name="form1" id="form1" onsubmit="return checkValidate()">
  	<ul>
			<li class="inputtext">
				<label>用户名</label>
               <input name="logname" type="text" id="logname" data-hint="用户名">
               
			</li>
			<li class="inputtext">
				<label>密 码</label>
                <input id="password" name="password" type="password" data-hint="密码">
				
			</li>
			<li class="inputtext1">
				<label>验证码</label>
				<input type="text" id="rand" name="rand" size="10" onblur="testRand()" onfocus="resetrandSpan()"/>&nbsp;
					<img alt="加载中" src="image.jsp" id="randImage" align="absmiddle">
					<a href="javascript:loadimage();"><font color=green>换一张</font></a><span id="testRand"></span>
				
			</li>
			
           	<li><label></label>
         		<input class="submit" type="submit" value="登录" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         		<input class="submit" type="button" value="注册" onclick="location.href='user/register.jsp'">
            </li>
         </ul>
     </form>
    </div>
   </div>
   
   
   </div>
   </div>
   </div>
   
  
  </body>
</html>
