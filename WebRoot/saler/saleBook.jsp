<%@ page contentType="text/html; charset=GBK" language="java" import="mybean.category" %>
<jsp:useBean id="typeList" class="mybean.category"></jsp:useBean>
<jsp:useBean id="book" class="mybean.BookBean"></jsp:useBean>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK" />
<title>新书上架</title>
<script  type="text/javascript"> 

  	function chkBN( ){
  	  	var obj = document.getElementById("bookName");
  	  	var v = obj.value;
  	  	if(v == "")
  	  	  	alert("新书名称不能为空！");
  	}

  	function setDate(){
  		var obj = document.getElementById("times");
  		var now = new Date();
  		var year= now.getFullYear();
  		var month=now.getMonth()+1;
  		var day=now.getDay();
  		obj.value = year.toString()+"-"+month.toString()+"-"+day.toString();
  	}

</script>
<style type="text/css">
.aaa{
	
}
table{
border-collapse:separate;
border-spacing:10px 12px;
}
table input{
	size:100px;
}


.STYLE1 {color: #FF0000}
.STYLE4 {font-size: x-large}

</style>
</head>

<body onLoad="setDate();">
<table width="1355" border="1" align="center">
  <tr>
    <th width="1345" height="92" bgcolor="#06b" scope="col" style="font-size:35px">&nbsp;<font color="white">卖书描述</font></th>
  </tr>
</table>

<div class="aaa">
<table width="761" border="0" align="center">
  <tr align="center">
    <td height="24" colspan="3" align="center" valign="top"><div align="center">带*号必须填写</div></td>
  </tr>
  <tr align="center">
    <td height="324" colspan="3" align="center" valign="top">
    <form id="" method="post" action="../bookAction1" enctype="multipart/form-data" >
      <table width="628" border="0" bgcolor="">
      <tr>
          <td align="right" valign="top">院系：</td>
          <td align="left" valign="top"><label>
			<select name="bookType" size="1" style="height:30px;width:200px"><%=typeList.getList()%></select></label>
		  </td>
        </tr>
        <tr>
          <td align="right" valign="top">专业：</td>
          <td align="left" valign="top"><label>
						<select name="major" size="1" style="height:30px;width:200px">
						  <option value="1" selected="selected">信息管理与信息系统</option>
						  <option value="2" >工程管理</option> 
						  <option value="3" >工程造价</option> 
						  <option value="4" >房产</option> 
						  <option value="5" >安全</option>
						</select> 
          </label></td>
        </tr>
        <tr>
          <td width="143" align="right" valign="center">*书籍名称：</td>
          <td width="471" align="left" valign="center">
			<input name="bookName" type="text" id="bookName" size="40" style="height:30px;width:300px" onBlur="chkBN();">
              <span class="STYLE1">*</span><br>
          </td>
        </tr>
		
        <tr>
          <td align="right" valign="center">出版社：</td>
          <td align="left" valign="center"><input name="press" type="text" id="press" size="20" style="height:30px;width:300px"></td>
        </tr>
        <tr>
          <td align="right" valign="center">单本价格：</td>
          <td align="left" valign="center"><input name="price" type="text" id="price" size="20" style="height:30px;width:300px"></td>
        </tr>
        <tr>
          <td align="right" valign="center">总本数：</td>
          <td align="left" valign="center"><input name="repertory" type="text" id="repertory" size="5" style="height:30px;width:300px"></td>
        </tr>

         <tr>
          <td align="right" valign="center">品相：</td>
          <td align="left" valign="center"><label>
						<select name="Commend" size="1" style="height:30px;width:200px"> 
						  <option value="1" selected="selected">九成新</option>
						  <option value="2" >八成新</option> 
						  <option value="3" >七成新</option> 
						  <option value="4" >六成新</option> 
						  <option value="5" >五成新</option>
						</select> 根据书的新旧情况评定
          </label></td>
        </tr>
        <tr>
          <td align="right" valign="center">卖书时间：</td>
          <td align="left" valign="center"><label>
            <input name="times" type="text" id="times" size="20" style="height:30px;width:200px">
            （例如：2008-03-04）
          </label></td>
        </tr>
        
        <tr>
          <td align="right" valign="center">上传图片：</td>
          <td align="left" valign="center"><label>
		        <input type="file" name="filename" id="filename">
			</label>
		  </td>
        </tr>
       
        <tr>
          <td align="right" valign="center">书的描述：</td>
          <td align="left" valign="center"><label>
            <textarea name="intro" id="intro" cols="25" rows="3"></textarea>
          （一百个汉字内）</label></td>
        </tr>
        
        <tr>
          <td align="right" valign="center">&nbsp;</td>
          <td align="left" valign="center"><label>
            <input type="submit" value="提交" >
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="reset" name="button2" id="button2" value="重设">
          </label></td>
        </tr>
      </table>
        </form>
    </td>
  </tr>
  
  
</table>
</div>

<p align="center"><a href="<%=request.getContextPath()%>/myweb.jsp">返回首页</a></p>
<p align="center"><a href="<%=request.getContextPath()%>/sale_major.jsp">根据专业卖书</a></p>
</body>
</html>
