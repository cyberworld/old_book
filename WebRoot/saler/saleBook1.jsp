<%@ page contentType="text/html; charset=GBK" language="java"
	import="mybean.category"%>
<jsp:useBean id="typeList" class="mybean.category"></jsp:useBean>
<jsp:useBean id="book" class="mybean.BookBean"></jsp:useBean>
<%-- <%
	int bid = Integer.parseInt(request.getParameter("bid"));
	if(!book.getBook(bid))
		return;
%> --%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK" />
		<title>我要卖书</title>
		<script type="text/javascript">
	var xmlHttp;
	function createXMLReq() {
		if (window.XMLHttpRequest) {
			xmlHttp = new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

	function sendPostReq(url, param) {
		createXMLReq();
		xmlHttp.open("POST", url, true);
		xmlHttp.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded; charset=UTF-8");
		xmlHttp.onreadystatechange = callback;
		xmlHttp.send(param);
	}

	function callback() {
		if (xmlHttp.readyState == 4) {
			if (xmlHttp.status == 200) {
				dowhat();
			} else {
				window.alert("page error! " + xmlHttp.status);
			}
		}
	}

	function dowhat() {
		if (xmlHttp.responseText.indexOf("2") != -1)
			alert("添加成功");
		else
			alert("添加失败");
	}

	function req() {
		var obj = document.getElementById("addbook");
		var bn = obj.bookName.value;
		var bt = obj.bookType.value;
		var ps = obj.press.value;
		var pc = obj.price.value;
		var rp = obj.repertory.value;
		var cm = obj.Commend.value;
		var tm = obj.times.value;
		var tr = obj.intro.value;

		var url = "../bookAction?act=add";
		var param = "bookName=" + bn + "&bookType=" + bt + "&press=" + ps
				+ "&price" + pc + "&repertory=" + rp + "&Commend=" + cm
				+ "&times=" + tm + "&intro=" + tr;
		sendPostReq(url, param);
	}

	function chkBN() {
		var obj = document.getElementById("bookName");
		var v = obj.value;
		if (v == "")
			alert("新书名称不能为空！");
	}

	function setDate() {
		var obj = document.getElementById("times");
		var now = new Date();
		var year = now.getFullYear();
		var month = now.getMonth() + 1;
		var day = now.getDay();
		obj.value = year.toString() + "-" + month.toString() + "-"
				+ day.toString();
	}
</script>
		<style type="text/css">
.STYLE1 {
	color: #FF0000
}

.STYLE4 {
	font-size: x-large
}
</style>
	</head>

	<body onLoad="setDate();">
		<table width="1355" border="1">
			<tr>
				<th width="1345" height="62" bgcolor="#00CC99" scope="col">
					&nbsp;
				</th>
			</tr>
		</table>
		<table width="1357" border="1" bgcolor="#336633">
			<tr>
				<th width="1347" scope="col">
					&nbsp;
				</th>
			</tr>
		</table>
		<table width="761" border="0" align="center">
			<tr>
				<td width="755" colspan="3" align="center">
					<span class="STYLE4">卖书描述</span>
				</td>
			</tr>
			<tr align="center">
				<td height="24" colspan="3" align="center" valign="top">
					<div align="left">
						带*号必须填写
					</div>
				</td>
			</tr>
			<tr align="center">
				<td height="324" colspan="3" align="center" valign="top">
					<form id="addbook" method="post" action="../bookAction" enctype="multipart/form-data">
						<table width="628" border="0" bgcolor="#CCCCCC">
							<tr>
								<td width="143" align="right" valign="top">
									*书籍名称：
								</td>
								<td width="471" align="left" valign="top">
									<input name="bookName" type="text" id="bookName" size="20"
										onBlur="chkBN();">
									<span class="STYLE1">*</span>
									<br>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">
									类型：
								</td>
								<td align="left" valign="top">
									<label>
										<select name="bookType" size="1"><%=typeList.getList()%></select>
									</label>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">
									出版社：
								</td>
								<td align="left" valign="top">
									<input name="press" type="text" id="press" size="20">
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">
									单本价格：
								</td>
								<td align="left" valign="top">
									<input name="price" type="text" id="price" size="20">
								</td>
							</tr>
							<!--<tr>
								<td align="right" valign="top">
									库存：
								</td>
								<td align="left" valign="top">
									<input name="repertory" type="text" id="repertory" size="5">
								</td>
							</tr>-->
							
							<tr>
								<td align="right" valign="top">
									品相：
								</td>
								<td align="left" valign="top">
									<label>
										<select name="Commend" size="1">
											<option value="1" selected="selected">
												九成新
											</option>
											<option value="2">
												八成新
											</option>
											<option value="3">
												七成新
											</option>
											<option value="4">
												六成新
											</option>
											<option value="5">
												五成新
											</option>
										</select>
									</label>
								</td>
							</tr>
							<tr>
								<td align="right" valign="top">
									卖书时间：
								</td>
								<td align="left" valign="top">
									<label>
										<input name="times" type="text" id="times" size="20">
										（例如：2008-03-04）
									</label>
								</td>
							</tr>



							<tr>
								<td align="right" valign="top">
									上传图片：
								</td>
								<td align="left" valign="top">
										<input type="file" name="filename" id="filename">
								</td>
							</tr>



							<tr>
								<td align="right" valign="top">
									书的描述：
								</td>
								<td align="left" valign="top">
									<label>
										<textarea name="intro" id="intro" cols="45" rows="5"></textarea>
										（一百个汉字内）
									</label>
								</td>
							</tr>

							<tr>
								<td align="right" valign="top">
									&nbsp;
								</td>
								<td align="left" valign="top">
									<label>
										<input type="button" name="button" id="button" value="提交" onClick=>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="reset" name="button2" id="button2" value="重设">
									</label>
								</td>
							</tr>
						</table>
					</form>
				</td>
			</tr>

		</table>
		<p align="center">
			<a href="<%=request.getContextPath()%>/myweb.jsp">返回首页</a>
		</p>
		<p align="center">
			<a href="<%=request.getContextPath()%>/sale_major.jsp">根据专业卖书</a>
		</p>
	</body>
</html>
