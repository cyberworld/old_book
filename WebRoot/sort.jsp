<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<jsp:useBean id="curBook" class="mybean.BookBean"></jsp:useBean>
<%
	String imgPath = request.getRealPath("/");
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="css/sidebar.css">
		<title>无标题文档</title>
		<style type="text/css">
html,body,div,span,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,address,big,cite,code,del,em,font,img,ins,small,strong,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend
	{
	margin: 0;
	padding: 0;
}

div {
	display: block;
}

a {
	text-decoration: none;
}

body {
	font: 12px/ 150% Arial, Verdana, "宋体b8b\4f53";
	color: #666;
}

li {
	list-style: none;
}

.box ul li {
	float: left;
	margin-right: 28px;
	margin-left: 28px;
}

.box {
	margin-top: 10px;
}

.title {
	border: 1px solid #CCCCCC;
	height: 40px;
	font-size: 16px;
	line-height: 40px;
	color: #CC3366;
	background: #FFFFCC;
}

#news_title a {
	display: block;
	position: absolute;
	top: 0px;
	right: 0px;
}

.box ul li a {
	display: block;
	width: 100px;
	text-align: center;
	/* [disabled]border: 1px solid #663300; */
}

.floor {
	clear: both;
	/* [disabled]height: 420px; */
	width: 800px;
}

#menu_main .item h3 {
	height: 30px;
	line-height: 30px;
	font-size: 14px;
	/* [disabled]color: #FFFFFF;
*/
	text-align: center;
}

#menu_main {
	border-right: 1px solid #CCCCCC;
	border-bottom: 1px solid #CCCCCC;
	border-left: 1px solid #CCCCCC;
	background: #3333FF;
	display: none;
}

#menu #all {
	height: 31.5px;
	width: 199px;
	border-right: 1px ridge #1d53bf;
	color: #FFFFFF;
	font-size: 16px;
	display: block;
	text-align: center;
	line-height: 30px;
}

#menu_main .item {
	background: #1a62a9;
	border-top: 1px solid #FFFFFF;
	border-bottom: 1px solid #FFFFFF;
	height: 30px;
}

.item h3:hover {
	background: #33CCCC;
}

#menu_main .item h3 a {
	color: #FFFFCC;
}

#page {
	margin: 0px auto;
	width: 1000px;
	position: relative;
}

#page_search {
	margin-top: 16px;
	position: relative;
}

#header {
	line-height: 30px;
	width: 100%;
	height: 30px;
	background: #F7F7F7;
	border-bottom: 1px solid #eee;
}

#page_header {
	height: 30px;
	margin: 0px auto;
	width: 1000px;
	text-align: right;
}

#logo {
	padding-top: 13px;
	height: 92px;
	width: 334px;
	float: left;
}

#SearchForm {
	position: relative;
	height: 35px;
	width: 428px;
	display: block;
	top: 34px;
}

#serach_area {
	border: 3px solid #1C6CBA;
	z-index: 0;
	position: absolute;
	display: inline-block;
	right: 0px;
	width: 100%;
}

#submitbox {
	display: inline;
	background: #0000CC;
}

.submit-input {
	background: #1C6CBA;
	height: 31px;
	width: 50px;
	color: #FFFFFF;
	margin-left: 0px;
	position: absolute;
	right: 0px;
	top: 0px;
	cursor: pointer;
}

#search_in {
	z-index: 2;
	display: inline-block;
	height: 27px;
	width: auto;
	position: absolute;
	left: 3px;
}

#search {
	width: 666px;
	height: 105px;
	margin-left: 334px;
	position: relative;
}

#form {
	position: relative;
	width: 378px;
	height: 33px;
}

#search_in_content {
	font-size: 14px;
	position: relative;
	top: 7px;
	left: 0px;
	display: inline-block;
	width: auto;
	white-space: nowrap;
	font-family: "Courier New", Courier, monospace;
}

#search_content {
	height: 25px;
	margin-left: 70px;
	display: inline-block;
	float: right;
	border-left: 2px solid #1C6CBA;
	font-size: 16px;
	line-height: 25px;
	border-top-style: none;
	border-right-style: none;
	border-bottom-style: none;
}

#search_down_arrow {
	background: url(img/serch_in.png) -130px -10px;
	height: 5px;
	width: 9px;
	position: absolute;
	top: 14px;
	display: block;
	right: -12px;
}

#searchDropdownBox {
	filter: alpha(opacity =                         0);
	opacity: 0;
	cursor: pointer;
	visibility: inherit;
	position: absolute;
	left: 0px;
	top: 6px;
	z-index: 1;
	width: 70px;
}

#sort {
	position: absolute;
	top: 40px;
	left: 200px;
}

#curbook {width ="853";height ="515";valign ="top";bgcolor ="#FFFFFF";
	
}

#advanced_search {
	position: relative;
	top: 5px;
	left: 430px;
}

#nav_main li {
	float: left;
	font-size: 16px;
}

#nav_main {
	position: absolute;
	top: 2px;
	left: 200px;
	width: 800px;
}

#nav {
	position: relative;
	height: 31.5px;
	background: url(images/nav.png) repeat center;
}

#nav_main li a {
	color: #FFFFFF;
	background: url(img/nav.png) center;
	display: block;
	width: 100px;
	height: 27px;
	text-align: center;
	line-height: 27px;
	border-right: 1px ridge #1d53bf;
	border-left: 1px ridge #1d53bf;
}

#nav_main li a:hover {
	background: #1d53bf;
	color: #FF3333;
}

#login_header {
	position: absolute;
	top: 0px;
	height: 30px;
	width: 1000px;
	text-align: right;
	margin: 0px auto;
}

#menu {
	position: absolute;
	/* [disabled]border: 2px solid #006699;
*/
	width: 197px;
}

#menu :hover #menu_main {
	display: block;
}

a:hover {
	color: #FF3333;
	text-decoration: underline;
}
</style>

		<script type="text/javascript">
function getbookclassify(){
	var url="";
	var json={
			"bookclassify":[
			{"classifyname":"计算机"},
			{"classifyname":"法律"},
			{"classifyname":"电力"},
			{"classifyname":"外语"},
			{"classifyname":"生活"},
			{"classifyname":"文学"},
			{"classifyname":"数学"},
			{"classifyname":"医学"},
			{"classifyname":"建筑"},
			{"classifyname":"其他"}
		]
		}
		return json;
}


</script>
	</head>

	<body>
		<div id="header">
			<div id="page_header">
				"您好，欢迎来到校园二手书网!"
			</div>

		</div>
		<div id="page">
			<div id="page_search">
				<div id="logo">
					<img src="images/logo.jpg" />
				</div>
				<div id="search">
					<form target="_top" action="" name="search" id="SearchForm">
						<div id="form">
							<span id="search_in"> <span id="search_in_content">
									全部分类 </span> <span id="search_down_arrow"> </span> <select name="url"
									id="searchDropdownBox" class="searchSelect" title="搜索范围"
									onchange="getselectvalue()" style="width: 70px">
									<option value="0" selected="selected">
										全部分类
									</option>
									<option value="1">
										计算机
									</option>
									<option value="2">
										管理
									</option>
									<option value="3">
										金融
									</option>
									<option value="4">
										经济
									</option>
									<option value="5">
										法律
									</option>
								</select> </span>
							<div id="serach_area">
								<input type="text" id="search_content" style="width: 300px"
									name="search_content" />
							</div>
						</div>
						<input type="submit" value="搜索" class="submit-input" title="搜索">
					</form>
					<a href="myweb.jsp" id="advanced_search">返回首页</a>
				</div>
			</div>
			<div id="nav">
				<div id="menu">
					<div id="all">
						全部书籍分类
					</div>
					<div id="menu_main" style="display: block">
						<div class="item">
							<h3>
								<a href="#jsj">计算机</a>
							</h3>
						</div>
						<div class="item">
							<h3>
								<a href="#gl">管理</a>
							</h3>
						</div>
						<div class="item">
							<h3>
								<a href="#jr">金融</a>
							</h3>
						</div>
						<div class="item">
							<h3>
								<a href="#jj">经济</a>
							</h3>
						</div>
						<div class="item">
							<h3>
								<a href="#fl">法律</a>
							</h3>
						</div>
					</div>
				</div>
				<div id="sort">
					<div class="floor">
						<div class="title">
							<h4 id="jsj">
								计算机
							</h4>
						</div>
						<div id="curbook" class="box"><%=curBook.getSortjsj(imgPath)%>
						</div>
					</div>
					
					<div class="floor">
						<div class="title">
							<h4 id="gl">
								管理学
							</h4>
						</div>
						<div id="curbook" class="box"><%=curBook.getSortgl(imgPath)%>
						</div>
					</div>
					
					<div class="floor">
						<div class="title">
							<h4 id="jr">
								金融学
							</h4>
						</div>
						<div id="curbook" class="box"><%=curBook.getSortjr(imgPath)%>
						</div>
					</div>
					
					<div class="floor">
						<div class="title">
							<h4 id="jj">
								经济学
							</h4>
						</div>
						<div id="curbook" class="box"><%=curBook.getSortjj(imgPath)%>
						</div>
					</div>
				</div>
				<ul id="nav_main">
					<li>
						<a href="#">首页</a>
					</li>
					<li>
						<a href="#">新书</a>
					</li>
					<li>
						<a href="#">求购</a>
					</li>
					<li>
						<a href="#">校内</a>
					</li>
					<li>
						<a href="#">论坛</a>
					</li>
				</ul>
			</div>
		</div>

	</body>
</html>

