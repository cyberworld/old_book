
<%@ page contentType="text/html; charset=GBK" language="java" import="mybean.UserBean"%>
<jsp:useBean id="user" class="mybean.UserBean"></jsp:useBean>
<%
	String log = request.getParameter("log");
	if(log == null)
		log = (String)session.getAttribute("logname");
	if(log == null){
		out.println("<script>alert('操作有误！');</script>");
		return;
	}
	user.getUser(log);
%><!DOCTYPE html>
<html lang="en">

<head>
        
  
    <meta http-equiv="Content-Type" content="text/html; charset=GBK" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.css">
    <title>用户信息</title>
        <style type="text/css">
  

            #student {
                margin-top: 20px;
            }
    
            #student table {
                margin-top: 20px;
            }
            p{
               text-align: center; 
            }
            header {
           padding: 10px;
            background-color: rgb(34, 92, 168);
            margin-bottom: 20px;
             margin-top: 20px;

    }.navbar {
	background-color: rgb(34, 92, 168); wdith：100px;
	padding: 10px;
	height: 80px;
	font-size: 40px;
	color: white;
}

.navbar p {
	font-family: 微软雅黑;
	text-align: center;
	background-color: 
}
        </style>
   
</head>

<body>
    
        <div class="container">
		<div class="navbar" >

			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#top-menu">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<p>
				用户详细信息
			</p>

		</div>

   
       
            <div class="tab-content">
                <div class="tab-pane active" id="student" >
                   
                    <table class="table table-striped table-bordered">
                            <tr>
                                    <td width="110" align="right">登录名：</td>
                                    <td width="227"><%=user.getLogname() %></td>
                                  </tr>
                                  <tr>
                                    <td align="right">真实姓名：</td>
                                    <td><%=user.getRealname() %></td>
                                  </tr>
                                  <tr>
                                    <td align="right">性别：</td>
                                    <td><%=user.getGender() %></td>
                                  </tr>
                                  <tr>
                                    <td align="right">Email：</td>
                                    <td><%=user.getEmail() %></td>
                                  </tr>
                                  <tr>
                                    <td align="right">电话：</td>
                                    <td><%=user.getPhone() %></td>
                                  </tr>
                                  <tr>
                                    <td align="right">地址：</td>
                                    <td><%=user.getAddress() %></td>
                                  </tr>
                                  <tr>
                                    <td align="right">省份：</td>
                                    <td><%=user.getProvince() %></td>
                                  </tr>
                                  <tr>
                                    <td align="right">爱好：</td>
                                    <td><%=user.getHobbies() %></td>
                                  </tr>
                                  <tr>
                                    <td align="right">教育：</td>
                                    <td><%=user.getEducation() %></td>
                                  </tr>
                                  <tr>
                                    <td align="right">个人简介：</td>
                                    <td><%=user.getSelfintro() %></td>
                                  </tr>
                        </table>
                </div>
                </div>
               
    
    
    <a href="../myweb.jsp">返回首页</a>
    <script src="../lib/jquery-3.2.1.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
</body>

</html>