<%@ page contentType="text/html;charset=GBK" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.css">
</head>
<style>
    #point {
        color: brown;
    }

    .point {
        text-align: right;
    }
    .submit{
        text-align:center;
    }
</style>

<body>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="" class="navbar-brand">新用户注册</a>
            </div>
        </div>

    </nav>
    <div class="container">
        <div class="row">
            <div class="tab-content">

                <form class="form-horizontal" method="post" action="../register">
                    <div class="point">注：带*项内容必须填写！</div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label" id="point">登陆名称* :</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" name="logname" id="logname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label" id="point">真实姓名* :</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" name="realname" id="realname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label" id="point">你的密码* :</label>
                        <div class="col-md-4">
                            <input class="form-control" type="password" name="psw" id="psw" pattern="[A-z0-9]{6}" placeholder="至少为6位数字或字母" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label" id="point">确认密码* :</label>
                        <div class="col-md-4">
                            <input class="form-control" type="password" name="again" id="again">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label" id="point">电子邮件* ：</label>
                        <div class="col-md-4">
                            <input class="form-control" type="email" name="email" id="email" placeholder="邮箱" autofocus required autocomplete="off" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label" id="point">性别* ：</label>
                        <div class="col-md-4">
                            <label class="radio-inline">
                                <input type="radio" name="gender" id="gender" value="男"> 男
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" id="gender" value="女"> 女
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label" id="point">联系地址* ：</label>
                        <div class="col-md-4">
                            <input class="form-control" name="address" type="text" id="address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label" id="point">联系电话* ：</label>
                        <div class="col-md-4">
                            <input class="form-control" name="phone" type="text" id="phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label">密码提示问题：</label>
                        <div class="col-md-4">
                            <input class="form-control" name="problem" type="text" id="problem">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label">提示答案：</label>
                        <div class="col-md-4">
                            <input class="form-control" name="answer" type="text" id="answer">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label">省份：</label>
                        <div class="col-md-4">
                            <select class="form-control" name="province" id="province">
                                <option value="NULL" selected="selected">请选择</option>
                                <option value="北京">北京</option>
                                <option value="上海">上海</option>
                                <option value="天津">天津</option>
                                <option value="重庆">重庆</option>
                                <option value="福建">福建</option>
                                <option value="辽宁">辽宁</option>
                                <option value="吉林">吉林</option>
                                <option value="河北">河北</option>
                                <option value="海南">海南</option>
                                <option value="陕西">陕西</option>
                                <option value="山西">山西</option>
                                <option value="甘肃">甘肃</option>
                                <option value="宁夏">宁夏</option>
                                <option value="新疆">新疆</option>
                                <option value="西藏">西藏</option>
                                <option value="青海">青海</option>
                                <option value="四川">四川</option>
                                <option value="云南">云南</option>
                                <option value="贵州">贵州</option>
                                <option value="湖南">湖南</option>
                                <option value="湖北">湖北</option>
                                <option value="河南">河南</option>
                                <option value="山东">山东</option>
                                <option value="安徽">安徽</option>
                                <option value="江苏">江苏</option>
                                <option value="浙江">浙江</option>
                                <option value="台湾">台湾</option>
                                <option value="香港">香港</option>
                                <option value="澳门">澳门</option>
                                <option value="广东">广东</option>
                                <option value="广西">广西</option>
                                <option value="江西">江西</option>
                                <option value="黑龙江">黑龙江</option>
                                <option value="内蒙古">内蒙古</option>
                                <option value="其他">其他</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label">兴趣爱好：</label>
                        <div class="col-md-4">
                            <input type="checkbox" name="hobbies" id="hobbies" value="a"> 计算机
                            <br>
                            <input type="checkbox" name="hobbies" id="hobbies" value="b"> 金融类
                            <br>
                            <input type="checkbox" name="hobbies" id="hobbies" value="c"> 法律类
                            <br>
                            <input type="checkbox" name="hobbies" id="hobbies" value="d"> 国际教育
                            <br>
                            <input type="checkbox" name="hobbies" id="hobbies" value="e"> 工商管理
                            <br>
                            <input type="checkbox" name="hobbies" id="hobbies" value="f"> 汉语言文学
                            <br>
                            <input type="checkbox" name="hobbies" id="hobbies" value="g"> 古典小说
                            <br>
                            <input type="checkbox" name="hobbies" id="hobbies" value="h"> 游戏
                            <br>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-5 control-label">自我介绍：</label>
                        <div class="col-md-4">
                            <textarea class="form-control" name="selfintro" id="selfintro" cols="45" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="submit">
                        <button type="submit" class="btn btn-primary">注册</button>
                        <button type="reset" class="btn btn-default">重置</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <script src="../lib/jquery-3.2.1.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
</body>

</html>