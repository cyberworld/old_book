<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK" import="mybean.orders"%>
<%
	String logname = (String)session.getAttribute("logname");
	if(logname == null || logname.equals("")){
		out.print("<script>alert('你还没有登录，请先登录！');self.location.replace('../myweb.jsp');</script>");
		return;
	}
%>
<jsp:useBean id="order" class="mybean.orders"></jsp:useBean>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.css">
    <meta http-equiv="Content-Type" content="text/html; charset=GBK" />
<title>购买记录</title>
<style type="text/css">
<!--
.STYLE3 {font-size: larger}
.STYLE4 {
	font-size: xx-large;
	color: #FF0000;
	font-weight: bold;
}
.STYLE6 {color: #0099FF}
.STYLE7 {color: #0099CC}
.STYLE8 {color: #FF0000}
-->

        #tou {
            padding: 10px;
            background-color: #afd6f7;
        }

        tr {
            text-align: center;
        }
        .navbar {
	background-color: rgb(34, 92, 168); wdith：100px;
	padding: 10px;
	height: 80px;
	font-size: 40px;
	color: white;
}

.navbar p {
	font-family: 微软雅黑;
	text-align: center;
	background-color: 
}
    </style>
</head>

<body>
    	<div class="container">
		<div class="navbar" >

			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#top-menu">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<p>
				订单详情如下：
			</p>

		</div>
        <div>
            <div width="1160">
                <table class="table  table-hover" width="1160" border="0" align="center" cellpadding="1" cellspacing="0">
                    <tr>
                    <td width="100" align="center">
                            <div align="center">
                                <span class="STYLE3">用户名</span>
                            </div>
                        </td>
                     <td width="100" align="center">
                            <div align="center">
                                <span class="STYLE3">姓名</span>
                            </div>
                        </td>
                        <td width="200" align="center">
                            <div align="center">
                                <span  class="STYLE3">书名称</span>
                            </div>
                        </td>
                        <td width="150" align="center">
                            <div align="center">
                                <span  class="STYLE3">出版社</span>
                            </div>
                        </td>
                        <td width="70" align="center">
                            <div align="center">
                                <span class="STYLE3">库存</span>
                            </div>
                        </td>
                       
                        <td width="83" align="center">
                            <div align="center">
                                <span class="STYLE3">单价</span>
                            </div>
                        </td>
                        <td width="74" align="center">
                            <div align="center">
                                <span class="STYLE3">数量</span>
                            </div>
                        </td>
                        <td width="102" align="center">
                            <div align="center">
                                <span class="STYLE3">订单情况</span>
                            </div>
                        </td>
                        <td width="102" align="center">
                            <div align="center">
                                <span class="STYLE3">操作</span>
                            </div>
                        </td>
                    </tr>
                    <tr>
	             <%=order.getBuyList(logname) %>
	             
                </table>
            </div>
        </div>
        <div>
            <div>
                <table class="table table-striped table-border">
                    <tr>
                        <th height="83" scope="col">
                            <div align="center">
                            <p>&nbsp;</p>
                                <span class="STYLE3">
                                   
                                    <p align="center">&nbsp; <a href="../manager/manager.jsp">&lt;&lt;Back </a> </p>
                                </span>
                            </div>
                        </th>
                    </tr>
                </table>
            </div>

        </div>
    </div>
    <script src="../lib/jquery-3.2.1.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
</body>

</html>