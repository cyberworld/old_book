<%@ page contentType="text/html; charset=GBK" language="java" import="mybean.BookBean" %>
<style type="text/css">
<!--
.STYLE25 {font-size: 16px; color: #990000; }
.STYLE26 {font-size: 16px}
.STYLE28 {font-size: 24px}
.STYLE29 {color: #FF0000}
-->
</style>
<%
	String logname = (String)session.getAttribute("logname");
	if(logname == null || logname.equals("")){
	%>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p class="STYLE29">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="STYLE28">你还没有登录，请先登录！</span></p>
		<p><span class="STYLE29">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="javascript:self.close()">【关闭】</a> &nbsp;&nbsp;
		<a href="javascript:self.location.replace('../user/login.jsp');">【登录】</a></span>
		  <%
		return;
	}
	String bid = request.getParameter("bid");
	BookBean book = new BookBean();
	book.getBook(Integer.parseInt(bid));
%>
</p>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>订单确认</title>
    <link rel="stylesheet" href="../lib/bootstrap/css/bootstrap.css">
    <style>
            #tou {
                padding: 10px;
                background-color: #afd6f7;
            }
    
        
        </style>
</head>
<body><div class="container" align="center" width="400">
        <form name="form1" method="post" action="../orderAction?bid=<%=bid%>&log=<%=logname%>&rp=<%=book.getRepertory()%>">
            <table class="table table-bordered" width="400" height="160"  >
              <tr>
                <td height="60" colspan="2" align="center" id="tou"><h1><font size="9" color="#337ab7" face="楷体">订单确认</font></h1></td>
              </tr>
              <tr>
                <td width="332" align="right"><span class="STYLE25">客户名字：</span></td>
                <td width="565"><span class="STYLE26"><%=(String)session.getAttribute("realname") %></span></td>
              </tr>
              <tr>
                <td align="right"><span class="STYLE25">书本名称：</span></td>
                <td ><span class="STYLE26"><%=book.getBookName() %></span></td>
              </tr>
              <tr>
                <td height="18" align="right"><span class="STYLE25">购买数量：</span></td>
                <td >        <span class="STYLE25">
                  <label>
                  <input name="number" type="text" id="number" value="1" size="6">
                  本</label>
                </span> </td>
              </tr>
              <tr>
                <td height="50" colspan="2" align="center"><label>
                  <input type="submit" name="button" class="btn btn-primary btn-lg active" value="确认">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="reset" name="button2"  class="btn btn-default btn-lg active" value="取消">
                </label></td>
              </tr>
            </table>
          </form>
        </div>
          <script src="../lib/jquery-3.2.1.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <a href="javascript:self.close()">【关闭】</a>
</body>
</html>