package myservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mybean.UserBean;

public class findpsw extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		
		String logname = request.getParameter("logname");
		response.setContentType("text/html; charset=GBK");      
	    PrintWriter out = response.getWriter();
		if(logname.indexOf("'")!=-1){
			out.println(" 用户名不合法！");
			return;
		}
		String problem = request.getParameter("problem");
		String answer = request.getParameter("answer");
		String msg = "";
		
		UserBean user = new UserBean();
		user.setLogname(logname);
		user.setProblem(problem);
		if(!msg.equals("OK")){
			out.println(" "+msg);
			return;
		}
		user.setAnswer(answer);
		if(!msg.equals("OK")){
			out.println(" "+msg);
			return;
		}
		msg = user.findPsw();
	    
		if(!msg.equals("OK")){
			out.println(" "+msg);
			return;
		}
		String psw = user.getPassword();
		out.println(psw);
	}

}
