package myservlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;


import mybean.UserBean;

public class logaction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
	    session.setAttribute("logname","");
		session.setAttribute("usertype","");
		session.setAttribute("realname","");
		response.sendRedirect("myweb.jsp");
	}

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		request.setCharacterEncoding("utf-8");
		String logname = request.getParameter("logname");
		String password = request.getParameter("password");
		String srand = request.getParameter("rand");
		String savecodes = request.getParameter("chkNum");

		String rand=(String)session.getAttribute("rand");
		int flag = 1;
		//String outRand="";
		
		response.setContentType("text/html; charset=GBK");      
		PrintWriter out = response.getWriter();
		
		if(savecodes != null){
			flag = 0;			
			String codes = (String)session.getAttribute("strEnsure");
		}
		if(!srand.equals(rand)){
			//outRand="验证码错误！请重新输入！";	
			//session.setAttribute("outRand",outRand);
			RequestDispatcher rd=request.getRequestDispatcher("/login.jsp");
			rd.forward(request, response);
			return;
		}
		
		UserBean checkUser = new UserBean();
		String msg = checkUser.CheckLogin(logname, password);
		
		if(msg.equals("OK")){
			
			String usertype = checkUser.getUsertype();
			session.setAttribute("logname", logname);
			session.setAttribute("usertype", usertype);
			session.setAttribute("realname", checkUser.getRealname());
			if(usertype.equals("1"))
				response.sendRedirect("manager/manager.jsp");
			else if(flag == 1)
				response.sendRedirect("myweb.jsp");
			else out.println("<script>alert('登录成功！');self.close()</script>");
		}else out.println(msg);
	}

}
