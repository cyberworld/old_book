package myservlet;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import mybean.UserBean;

public class register extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		
		String log = new String(request.getParameter("log").getBytes("iso-8859-1"),"GBK");
	    response.setContentType("text/html; charset=GBK");      
		PrintWriter out = response.getWriter();
		UserBean user = new UserBean();
		if(user.UserExist(log))
			out.println("1");
		else out.println("2");
	}
	

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("GBK");
		String logname = request.getParameter("logname");
		String realname = request.getParameter("realname");
		String psw = request.getParameter("psw");
		String confirm = request.getParameter("again");
		String email = request.getParameter("email");
		String gender = request.getParameter("gender");
		String address = request.getParameter("address");
		String phone = request.getParameter("phone");
		String problem = request.getParameter("problem");
		String answer = request.getParameter("answer");
		String province = request.getParameter("province");
		String education = request.getParameter("education");
		String hobbies[] = request.getParameterValues("hobbies");
		String selfintro = request.getParameter("selfintro");

		String msg;
		UserBean Register = new UserBean();
	    response.setContentType("text/html; charset=GBK");      
	    PrintWriter out = response.getWriter();  
		

		Register.setLogname(logname);
		msg = Register.checkLogname();
		if(!msg.equals("OK")){
			out.println(msg);
			return;
		}
		
		Register.setRealname(realname);
		if(!msg.equals("OK")){
			out.println(msg);
			return;
		}
		Register.setPassword(psw);
		Register.setConfirm(confirm);
		msg = Register.checkPassword();
		if(!msg.equals("OK")){
			out.println(msg);
			return;
		}
		Register.setEmail(email);
		msg = Register.checkEmail();
		if(!msg.equals("OK")){
			out.println(msg);
			return;
		}
		Register.setGender(gender);
		Register.setAddress(address);
		if(!msg.equals("OK")){
			out.println(msg);
			return;
		}
		Register.setPhone(phone);
		if(!msg.equals("OK")){
			out.println(msg);
			return;
		}
		Register.setProblem(problem);
		if(!msg.equals("OK")){
			out.println(msg);
			return;
		}
		Register.setAnswer(answer);
		if(!msg.equals("OK")){
			out.println(msg);
			return;
		}
		Register.setProvince(province);
		Register.setEducation(education);
		
		String hobby = "";
		for(int i = 0; hobbies != null && i < hobbies.length; i++)
			hobby += hobbies[i];
		Register.setHobbies(hobby);
		
		Register.setSelfintro(selfintro);
		if(!msg.equals("OK")){
			out.println(msg);
			return;
		}
		Register.setUsertype("2");
		if(!Register.addUser())
			out.println("ע�᲻�ɹ���");
		else response.sendRedirect("user/reg_sec.jsp");
	}

}
