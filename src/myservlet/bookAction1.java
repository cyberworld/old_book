package myservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import mybean.BookBean;

public class bookAction1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		//String action = request.getParameter("act");
	    response.setContentType("text/html; charset=GBK");      
	    PrintWriter out = response.getWriter();

	   
		boolean isMultipart=ServletFileUpload.isMultipartContent(request);
		System.out.println(isMultipart);
		String fileName="";
		
		String filePath = request.getRealPath("/");//取当前系统路径  
		System.out.println("------filePath:"+filePath);
		
		if(isMultipart){

			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();

			// Configure a repository (to ensure a secure temp location is used)
			ServletContext servletContext = this.getServletConfig().getServletContext();
			File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
			factory.setRepository(repository);

			
			// Set factory constraints
			factory.setSizeThreshold(4*1024);
	
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			
			// Set overall request size constraint
			upload.setSizeMax(4*1024*1024);			
		
		// Parse the request
			try {
				List<FileItem> items = upload.parseRequest(request);
				
				
				String path=this.getServletConfig().getServletContext().getRealPath("/img/");
				System.out.println("---------path:"+path);			
				
				// Process the uploaded items
				Iterator<FileItem> iter = items.iterator();
				while (iter.hasNext()) {
				    FileItem item = iter.next();
	
				    if (item.isFormField()) {
				    	String name = item.getFieldName();
				        String value = item.getString();
				        
				        System.out.println(name+":"+value);
				    } else {
				    	String fieldName = item.getFieldName();
				        fileName = item.getName();
				        String contentType = item.getContentType();
				        boolean isInMemory = item.isInMemory();
				        long sizeInBytes = item.getSize();
				        System.out.println(fieldName+":"+fileName+":"+contentType+":"+isInMemory+":"+sizeInBytes);
				       
				     // Process a file upload
				        if (fileName!=null) {
				        	
				            File uploadedFile = new File(path,fileName);//拼接
				            try {
								item.write(uploadedFile);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				        } else {
	//			            InputStream uploadedStream = item.getInputStream();
	//			       
	//			            uploadedStream.close();
				        }
				    }
				}
			} catch (FileUploadException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	    
	    
	    
	    
		//if(action.equals("add")){
			String bookName = request.getParameter("bookName");
			String bookType = request.getParameter("bookType");
			String press = request.getParameter("press");
			String price = request.getParameter("price");
			String repertory = request.getParameter("repertory");
			String times = request.getParameter("times");
			String intro = request.getParameter("intro");
			String Commend = request.getParameter("Commend");
			
			BookBean curbook = new BookBean();
			
			curbook.setBookName(bookName);
			curbook.setBookType(bookType);
			curbook.setPress(press);
			curbook.setPrice(price);
			curbook.setRepertory(repertory);
			curbook.setTimes(times);
			curbook.setIntro(intro);
			curbook.setCommend(Commend);
			curbook.setImg(fileName);
			if(curbook.addSaleBook())
				out.println("1");
			else out.println("2");
		//}
		

		
		
	}

}
