package myservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mybean.BookBean;

public class bookAction extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("act");
	    response.setContentType("text/html; charset=GBK");      
	    PrintWriter out = response.getWriter();

		if(action.equals("add")){
			String bookName = request.getParameter("bookName");
			String bookType = request.getParameter("bookType");
			String press = request.getParameter("press");
			String price = request.getParameter("price");
			String repertory = request.getParameter("repertory");
			String times = request.getParameter("times");
			String intro = request.getParameter("intro");
			String Commend = request.getParameter("Commend");
			
			BookBean curbook = new BookBean();
			
			curbook.setBookName(bookName);
			curbook.setBookType(bookType);
			curbook.setPress(press);
			curbook.setPrice(price);
			curbook.setRepertory(repertory);
			curbook.setTimes(times);
			curbook.setIntro(intro);
			curbook.setCommend(Commend);
			if(curbook.addBook())
				out.println("1");
			else out.println("2");
		}
	}

}
