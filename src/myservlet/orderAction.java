package myservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mybean.orders;

public class orderAction extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String logname = request.getParameter("log");
		String bid = request.getParameter("bid");
		int number = Integer.parseInt(request.getParameter("number"));
		response.setContentType("text/html; charset=GBK");
		PrintWriter out = response.getWriter();

		int rp = Integer.parseInt(request.getParameter("rp"));
		if (number > rp) {
			out.println("<script>alert('库存不足！');history.back();</script>");
			return;
		}

		orders order = new orders();
		order.addOrder(logname, bid, number);
		out.println("<script>alert('提交成功！');self.close();</script>");
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String logname = request.getParameter("log");
		String bid = request.getParameter("bid");
		String action = request.getParameter("act");
		orders order = new orders();
		response.setContentType("text/html; charset=GBK");
		PrintWriter out = response.getWriter();

		if (action.equals("remove")) {
			order.removeOrder(logname, bid);
			out
					.println("<script>alert('移除成功！');self.location.replace('orders/orderlst.jsp');</script>");
		} else if (action.equals("del")) {
			order.delOrder(logname, bid);
			out
					.println("<script>alert('删除成功！');self.location.replace('orders/orderlst.jsp');</script>");
		}else if (action.equals("clear")) {
			order.clearOrder(logname, bid);
			out
					.println("<script>alert('清除成功！');self.location.replace('orders/orderlst.jsp');</script>");
		}
		else if (action.equals("buy")) {
			order.buyOrder(logname);
			order.kuOrder(logname);
			order.clearOrder(logname, bid);
			out.println("<script>alert('购买成功！');self.location.replace('orders/orderlst.jsp');</script>");
		}else if (action.equals("acp")) {
			order.acpBuy(logname,bid);
			out
					.println("<script>alert('处理成功！');self.location.replace('orders/list.jsp');</script>");
		}
	}
	// else if(action.equals("remove")){
	// ArrayList buylist=(ArrayList)session.getAttribute("goodslist");
	// for(int i=0;i<buylist.size();i++){
	// GoodsSingle temp = (GoodsSingle) buylist.get(i);
	// if (temp.getName().equals(MyTools.toChinese("name"))) {
	// temp.setNum(temp.getNum()-1);
	// }
	// }
	// String name= request.getParameter("name");
	// myCar.removeItem(name);
	// response.sendRedirect("shopcar.jsp");
	// }
	// else if(action.equals("clear")){
	// ArrayList buylist=(ArrayList)session.getAttribute("goodslist");
	// for(int i=0;i<buylist.size();i++){
	// GoodsSingle temp = (GoodsSingle) buylist.get(i);
	// if (temp.getName().equals(MyTools.toChinese("name"))) {
	// temp.setNum(0);
	// }

}
