package myservlet;

import java.io.*;
import java.sql.*;

import javax.servlet.*;
import javax.servlet.http.*;
import com.jspsmart.upload.*;

import mybean.BookBean;

public class ImgUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ServletConfig config;

	final public void init(ServletConfig config) throws ServletException {
		this.config = config;
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		int bid = Integer.parseInt(request.getParameter("bid"));
		response.setContentType("text/html;charset=GBK");
		PrintWriter out = response.getWriter();
		request.setCharacterEncoding("GBK");
					
		String path=request.getRealPath("/");
		SmartUpload mySmartUpload = new SmartUpload();
							
		mySmartUpload.initialize(config,request,response);
		try {
			mySmartUpload.upload();
			String filename = mySmartUpload.getFiles().getFile(0).getFileName();						
			mySmartUpload.save(path);
			BookBean book = new BookBean();
			book.setImage(path+filename, bid);
		} catch (SmartUploadException e1) {
			e1.printStackTrace();
		}
		

		
	}
}
