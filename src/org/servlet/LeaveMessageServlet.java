package org.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import mybean.*;

public class LeaveMessageServlet extends HttpServlet {
	message stu = null;
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		String userid =req.getParameter("userid");

	    String action= req.getParameter("action");

	    message stu = null;
        int m=0;
	   
	    if ("new".equalsIgnoreCase(action)) {
		stu = doNew(req, res);
		 res.sendRedirect("message_success.jsp");}
	    if ("delete".equalsIgnoreCase(action)) {
	    	try{
	    		m = doDelete(userid);
	    	 res.sendRedirect("getmain.jsp");}
	    	catch(SQLException e){}
	    }
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		doGet(req,res);
		
	}

	public message doNew(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {

		message stu = new message();

		String userid = req.getParameter("userid");

		String name = req.getParameter("name");

		String address = req.getParameter("address");

		String phone = req.getParameter("phone");

		String dep = req.getParameter("dep");

		String bookname = new String(req.getParameter("bookname").getBytes(
				"ISO8859_1"));

		String press = new String(req.getParameter("press").getBytes(
				"ISO8859_1"));
		String number = new String(req.getParameter("number").getBytes(
				"ISO8859_1"));
		String day = new String(req.getParameter("day").getBytes("ISO8859_1"));

		stu.setUserid(userid);

		stu.setName(name);

		stu.setAddress(address);

		stu.setPhone(phone);

		stu.setDep(dep);

		stu.setBookname(bookname);
		stu.setPress(press);
		stu.setNumber(number);
		stu.setDay(day);

		stu.addMessage();

		return stu;

	}
	public int doDelete(String userid) throws SQLException {

	      int num=0;

	    message stu=new message();

	    num=stu.deleteMessage(userid);

	    return num;

	  }
	

}
