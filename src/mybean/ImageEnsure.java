package mybean;
import java.io.OutputStream;
import java.awt.image.*;
import java.awt.*;
import java.util.*;
import javax.imageio.*;
import java.io.IOException;

public class ImageEnsure
{
	public ImageEnsure()
	{

	}
	 public Color getRandColor(int fc,int bc){//给定范围获得随机颜色
		   Random random = new Random();
		   if(fc>255) fc=255;
		   if(bc>255) bc=255;
		   int r=fc+random.nextInt(bc-fc);
		   int g=fc+random.nextInt(bc-fc);
		   int b=fc+random.nextInt(bc-fc);
		   return new Color(r,g,b);
	 }
	private char mapTable[]=
	{
		'A','B','C','D','E','F',
		'G','H','I','J','K','L',
		'M','N','O','P','Q','R',
		'S','T','U','V','W','X',
		'Y','Z','0','1','2','3',
		'4','5','6','7','8','9'
	};

	public String getEnsure(int width,int height,OutputStream os)
	{
		if(width<=0)width=60;
		if(height<=0)height=20;

		BufferedImage image=new BufferedImage(width,height,BufferedImage.TYPE_INT_BGR);
//获取图形上下文
		Graphics g=image.getGraphics();
//设定背景颜色
		g.setColor(getRandColor(220,250));
		g.fillRect(0, 0, width, height);
//画边框
		g.setColor(getRandColor(200,250));
		g.drawRect(0, 0, width-1, height-1);
//取随机产生的认证码
		String strEnsure="";
//4代表4位验证码
		for(int i=0; i<4; ++i)
		{
			strEnsure+=mapTable[(int)(mapTable.length*Math.random())];
		}
//将认证码显示到图像中
		g.setColor(getRandColor(160,200));
		g.setFont(new Font("Atlantic Inline",Font.PLAIN,18));
		String str=strEnsure.substring(0,1);
		g.drawString(str, 8, 17);

		str=strEnsure.substring(1,2);
		g.drawString(str, 20, 15);
		str=strEnsure.substring(2,3);
		g.drawString(str, 35, 18);

		str=strEnsure.substring(3,4);
		g.drawString(str, 45, 15);
//随机产生100个干扰点
		Random rand=new Random();
		for(int i=0; i<100; i++)
		{
			int x=rand.nextInt(width);
			int y=rand.nextInt(height);
			g.drawOval(x, y, 1, 1);
		}
//释放图形上下文
		g.dispose();
		try
		{
//输出图像到页面
			ImageIO.write(image, "JPEG", os);
		}
		catch(IOException e)
		{
			return "";
		}
		return strEnsure;
	}
}