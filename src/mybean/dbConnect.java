package mybean;
import java.sql.*;


public class dbConnect {
	Connection conn = null;
	Statement stmt = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	public dbConnect() {
		String url = "jdbc:mysql://localhost:3306/test?user=root&password=root&useUnicode=true&characterEncoding=UTF-8";
		
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(url);
			stmt= conn.createStatement();
		}catch(Exception e) {
			System.err.println("数据库连接错误: " + e.getMessage());
		}
	}	
	public ResultSet executeQuery(String sql) {
		try {
			rs = stmt.executeQuery(sql);
		}catch(SQLException ex) { 
			System.err.println("执行查询错误 : " + ex.getMessage());
		}
		return rs;
	}

	public PreparedStatement getPstmt(String sql){
		
		try {
			if(stmt != null)stmt.close();
			pstmt = conn.prepareStatement(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pstmt;
	}

	public int executeUpdate(String sql) {
		int affectRows=-1;
		try {
			affectRows = stmt.executeUpdate(sql);
		}catch(SQLException ex) { 
			System.err.println("执行更新错误 : " + ex.getMessage());
		}
		return affectRows;
	}
public int executeInsert(String sql){ 
		
		int num=0;
		try{ 
		    num=stmt.executeUpdate(sql); 
		} catch(SQLException ex){ 
			System.err.println("执行插入有错误:"+ex.getMessage() ); 
		}
		return num;
}
}
