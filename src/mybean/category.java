package mybean;
import java.sql.*;

public class category {
	
	private String type;
	
	public String getList(){
		try {
			dbConnect db = new dbConnect();
			ResultSet rs = db.executeQuery("select * from category");		
			String mylst = "";
			while(rs.next()){
				mylst += "<option value=\""+ rs.getInt("id") +"\">"+rs.getString("type")+"</option>";
			}
			
			rs.close();
			return mylst;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getType(String id){
		String sql = "select type from category where id="+id;
		dbConnect db = new dbConnect();
		ResultSet rs = db.executeQuery(sql);
		try {
			if(!rs.next())return null;
			type = rs.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return type;
	}

}
