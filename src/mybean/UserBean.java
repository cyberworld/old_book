package mybean;
import java.sql.*;

public class UserBean{

	private String logname;
	private String realname;
	private String pass;
	private String confirm;
	private String email;
	private String gender;
	private String address;
	private String phone;
	private String problem;
	private String answer;
	private String province;
	private String education;
	private String hobbies;
	private String selfintro;
	private String usertype;

	public void setLogname(String logname){
		this.logname = logname;
	}

	public String getLogname(){
		return logname;
	}

	public void setRealname(String realname){
		this.realname = realname;
	}

	public String getRealname(){
		return realname;
	}

	public void setPassword(String pass){
		this.pass = pass;
	}
	public void setConfirm(String confirm)
	{
		this.confirm = confirm;
	}

	public String getPassword(){
		return pass;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setProblem(String problem){
		this.problem = problem;
	}

	public String getProblem(){
		return problem;
	}

	public void setAnswer(String answer){
		this.answer = answer;
	}

	public String getAnswer(){
		return answer;
	}

	public void setProvince(String province){
		this.province = province;
	}

	public String getProvince(){
		return province;
	}

	public void setEducation(String education){
		this.education = education;
	}

	public String getEducation(){
		return education;
	}

	public void setHobbies(String hobbies){
		this.hobbies = hobbies;
	}

	public String getHobbies(){
		return hobbies;
	}

	public void setSelfintro(String selfintro){
		this.selfintro = selfintro;
	}

	public String getSelfintro(){
		return selfintro;
	}

	public void setUsertype(String usertype){
		this.usertype = usertype;
	}

	public String getUsertype(){
		return usertype;
	}

	public String checkLogname( ){
		if(UserExist(logname))
			return "用户" + logname + "已存在！";
		return "OK";
	}


	public String checkPassword( ){
		if(pass.length() < 6)
			return "密码至少要6位";
		if(pass.indexOf("'") != -1)
			return "密码含有不合法字符";
		if(!pass.equals(confirm))
			return "两次密码不一致";
		return "OK";
	}

	public String checkEmail( ){
		if(email.equals("")){
			return "邮箱不能为空！";
		}
		if(email.indexOf("'") != -1)
			return "电子邮箱含有不合法字符";
		int addrx = email.indexOf('@');
		int dotx = email.indexOf('.');
		if(addrx < 0 || dotx < 0 || addrx > dotx)
			return "请注意电子邮箱格式！";
		return "OK";
	}








  public boolean UserExist(String user){
	dbConnect db = new dbConnect();
	boolean result;
	ResultSet rs = db.executeQuery("select * from userid where logname='" + user + "'");
	try {
		if(rs.next()){ 
			result = true;
		}else{
			result = false;
		}
		rs.close();
		return result;
	} catch (SQLException e) {
		e.printStackTrace();
		return false;
	}
  }
  
  public boolean addUser(){
	dbConnect db = new dbConnect();
	String sql = "insert into userid values('"+ logname +"','"+ realname +"','"+ pass +"','"+ email +"','"+ gender +"','"+ address +"','"+ phone +"','"+ problem +"','"+ answer +"','"+ province +"','"+ education +"','"+ hobbies +"','"+ selfintro +"',"+ usertype +")";
	int s = db.executeUpdate(sql);
	
	if(s > 0) return true;
	return false;
  }
  
  public boolean DelUser(String user){
	if(!UserExist(user))return false;
	dbConnect db = new dbConnect();
	int s = db.executeUpdate("delete from userid where logname='" + user + "'");

	if(s > 0) return true;
	return false;
  }
  
  public boolean getUser(String user){
	if(!UserExist(user)) return false;
	dbConnect db = new dbConnect();
	ResultSet rs = db.executeQuery("select * from userid where logname='" + user + "'");
	
	try {
		if(!rs.next())return false;
		logname = rs.getString("logname");
		realname = rs.getString("realname");
		email = rs.getString("email");
		gender = rs.getString("gender");
		address = rs.getString("address");
		phone = rs.getString("phone");
		problem = rs.getString("problem");
		answer = rs.getString("answer");
		province = rs.getString("province");
		education = rs.getString("education");
		hobbies = rs.getString("hobbies");
		selfintro = rs.getString("selfintro");
		usertype = rs.getString("type");
		rs.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return true;
  }

  public boolean UpdateUser(){
	dbConnect db = new dbConnect();
	String sql = "update userid set realname='"+ realname +"',email='"+ email +"',gender='"+ gender +"',address='"+ address +"',phone='"+ phone +"',province='"+ province +"',education='"+ education +"',bobbies='"+ hobbies +"',selfintro='"+ selfintro +"',type="+ usertype +",where logname='"+ logname +"'";
	int s = db.executeUpdate(sql);
	if(s > 0) return false;
	else return true;
  }  
  
  public String CheckLogin(String user, String psw){
	dbConnect db = new dbConnect();
	ResultSet rs = db.executeQuery("select * from userid where logname='" + user + "'");
	try {
		if(!rs.next()){ 
			return "用户名" + user + "不存在!";
		}
		if(!psw.equals(rs.getString("password"))){
			return "密码错误!";
		}
		usertype = rs.getString("type");
		realname = rs.getString("realname");
		rs.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return "OK";
  }
  
  public boolean changePassword(){
	String sql = "update userid set password='" + pass + "' where logname='" + logname + "'";
	dbConnect db = new dbConnect();
	int s = db.executeUpdate(sql);
	if(s > 0) return false;
	return true;
  }
  
  public String findPsw( ){
	  dbConnect db = new dbConnect();
	  try{
		  ResultSet rs = db.executeQuery("select * from userid where logname='" + logname + "'");
		  if(!rs.next()) return "用户"+logname+"不存在";
		  String pro = rs.getString("problem");
		  String ans = rs.getString("answer");
		  if(!problem.equals(pro))
				return "密码提示问题错误！";
			if(!answer.equals(ans))
				return "密码提示答案有误！";
			pass = rs.getString("password");
			rs.close();
			return "OK";
	  }catch(Exception ex){
		  return "数据库操作异常！";
	  }
  }
  
  public String getUserList( ){

	  String lst = "";
	  String sql = "select logname,realname,email,address,phone from userid where type=2";	  
	  
	  dbConnect db = new dbConnect();
	  ResultSet rs = db.executeQuery(sql);
	  try {
			while(rs.next()){
				String log = rs.getString(1);
				lst += "<tr align=\"left\">\n";
				lst += "<td>"+ log +"</td>\n";
				lst += "<td>"+ rs.getString(2) +"</td>\n";
				lst += "<td>"+ rs.getString(3) +"</td>\n";
				lst += "<td>"+ rs.getString(4) +"</td>\n";
				lst += "<td>"+ rs.getString(5) +"</td>\n";
				lst += "<td><a href=\"../userAction?log="+ log +"\">删除</a></td>\n";
			}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	  return lst;
	  
  }
  
}
