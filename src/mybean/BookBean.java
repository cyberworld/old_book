package mybean;
import java.sql.*;
import java.io.*;

public class BookBean{
	
	private String bookId;
	private String bookName;
	private String bookType;
	private String autho;
	private String press;
	private String price;
	private String repertory;
	private String times;
	private String intro;
	private String Commend;
	private InputStream Images;
	
	private String img;
	private String check;

	public void setBookId(String bookId){
		this.bookId = bookId;
	}

	public String getBookId( ){
		return bookId;
	}

	public void setBookName(String bookName){
		this.bookName = bookName;
	}

	public String getBookName( ){
		return bookName;
	}

	public void setBookType(String bookType){
		this.bookType = bookType;
	}
	
	public String getBookType(int act){
		if(act == 1)
			return bookType;
		else if(act == 2){
			category type = new category();
			return type.getType(bookId);			
		}
		return null;
	}

	public void setAutho(String autho){
		this.autho = autho;
	}

	public String getAutho( ){
		return autho;
	}

	public void setPress(String press){
		this.press = press;
	}

	public String getPress( ){
		return press;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice( ){
		return price;
	}

	public void setRepertory(String repertory){
		this.repertory = repertory;
	}

	public String getRepertory( ){
		return repertory;
	}

	public void setTimes(String times){
		this.times = times;
	}

	public String getTimes( ){
		return times;
	}

	public void setIntro(String intro){
		this.intro = intro;
	}

	public String getIntro( ){
		return intro;
	}

	public void setImage(String imgPath, int id){
		
		String sql = "update book set Images = ? where id = ?";
		dbConnect db = new dbConnect();
		
		File img = new File(imgPath);
		
		try {
			Images = new FileInputStream(img);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		PreparedStatement pstmt = db.getPstmt(sql);
		try {
			pstmt.setBinaryStream(1, Images, Images.available());
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
			pstmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		img.delete();
	}

	public void showImage(String path, int bid){
		try {
			if(Images == null)return;
			path += "images/"+ bid+".jpg";
			FileOutputStream sout;
			sout = new FileOutputStream(path);
			byte b[] = new byte[Images.available()];
			Images.read(b);
			sout.write(b);
			sout.flush();
			sout.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void setCommend(String Commend){
		this.Commend = Commend;
	}

	public String getCommend( ){
		return Commend;
	}

  public boolean BookExist(String bookId){
	dbConnect db = new dbConnect();
	ResultSet rs = db.executeQuery("select * from book where id=" + bookId);
	try {
		if(rs.next()){ 
			rs.close();
			return true; 
		}else{
			rs.close(); 
			return false;
		}
	} catch (SQLException e) {
		e.printStackTrace();
		return false;
	}
  }
  
  public boolean addBook(){
	dbConnect db = new dbConnect();
	String sql = "insert into book(name,type,press,price,repertory,times,intro,Commend) values('"
				+bookName +"',"+ bookType +",'"+ press +"',"+ price +","+ repertory +",'"+ times +"','"+ intro +"'," + Commend+")";
	int s = db.executeUpdate(sql);
	if(s > 0) return false;
	return true;
  }
  
  public boolean addSaleBook(){
		dbConnect db = new dbConnect();
		String sql = "insert into salebook(name,type,press,price,repertory,times,intro,Commend,img,check) values('"
					+bookName +"',"+ bookType +",'"+ press +"',"+ price +","+ repertory +",'"+ times +"','"+ intro +"'," + Commend+",'"+ img +"','0')";
		int s = db.executeUpdate(sql);
		if(s > 0) return false;
		return true;
	  }
  
  public boolean DelBook(String bookId){
	if(!BookExist(bookId))return false;
	dbConnect db = new dbConnect();
	int s = db.executeUpdate("delete from book where id=" + bookId);
	if(s > 0) return false;
	return true;

  }
  
  public boolean getBook(int bookid){
	dbConnect db = new dbConnect();
	String sql = "select * from book where id="+ bookid;
	ResultSet rs = db.executeQuery(sql);

	try {
		if(!rs.next())return false;
		bookId = rs.getString("id");
		bookName = rs.getString("name");
		bookType = rs.getString("type");
		autho = rs.getString("autho");
		press = rs.getString("press");
		price = rs.getString("price");
		repertory = rs.getString("repertory");
		times = rs.getString("times");
		intro = rs.getString("intro");
		Images = rs.getBinaryStream("Images");
		Commend = rs.getString("Commend");
		rs.close();
		return true;
	} catch (SQLException e) {
		e.printStackTrace();
		return false;
	}
  }

  public boolean UpdateBook(){
	dbConnect db = new dbConnect();
	String sql = "update book set Commend="+ Commend +",name='"+ bookName +"',type="+ 
				bookType +",autho='"+ autho +"',press='"+ press +"',price="+ price +
				",repertory="+ repertory +",times='"+ times +"',intro='"+ intro +"' where id="+ bookId;
	int s = db.executeUpdate(sql);
	if(s > 0) return false;
	else return true;
  }
  
  public String getCommendTable(String path){
	  String table = "<br/><table width=\"800\" border=\"0\" align=\"center\">";
	  dbConnect db = new dbConnect();
	  ResultSet rs = db.executeQuery("select id,name,Images from book where Commend=1");
	  int i = 0;
	  try {
		while(rs.next()){
			  if(i%4 == 0) table += "<tr>";
			  int id = rs.getInt("id");
			  String name = rs.getString("name");
			  String url = "book/information.jsp?bid=" + id;
			  table += "<td width=\"300\" height=\"150\" align=center ><img src=\"images/"+ id 
			  			+".jpg\" width=\"80\" height=\"120\" /><br/><a href=\"" +  url + "\" >"+ name 
			  			+"</a></td>";
			  if(i%4 == 3) table += "</tr>";
			  Images = rs.getBinaryStream("Images");
			  showImage(path, id);
			  i++;
		  }
		  rs.close();
	  } catch (SQLException e) {
		  e.printStackTrace();
	  }
	  table += "</table>";
	  return table;
  }
  public String getSortjsj(String path) {
	  String table = "<br/><table width=\"800\" border=\"0\" align=\"center\">";
	  dbConnect db = new dbConnect();
	  ResultSet rs = db.executeQuery("select id,name,Images from book where Commend=1");
	  int i = 0;
	  try {
		while(rs.next()){
			  if(i%4 == 0) table += "<tr>";
			  int id = rs.getInt("id");
			  String name = rs.getString("name");
			  String url = "book/information.jsp?bid=" + id;
			  table += "<td width=\"300\" height=\"150\" align=center ><img src=\"images/"+ id 
			  			+".jpg\" width=\"80\" height=\"120\" /><br/><a href=\"" +  url + "\" >"+ name 
			  			+"</a></td>";
			  if(i%4 == 3) table += "</tr>";
			  Images = rs.getBinaryStream("Images");
			  showImage(path, id);
			  i++;
		  }
		  rs.close();
	  } catch (SQLException e) {
		  e.printStackTrace();
	  }
	  table += "</table>";
	  return table;
  }
  public String getSortgl(String path) {
	  String table = "<br/><table width=\"800\" border=\"0\" align=\"center\">";
	  dbConnect db = new dbConnect();
	  ResultSet rs = db.executeQuery("select id,name,Images from book where Commend=2");
	  int i = 0;
	  try {
		while(rs.next()){
			  if(i%4 == 0) table += "<tr>";
			  int id = rs.getInt("id");
			  String name = rs.getString("name");
			  String url = "book/information.jsp?bid=" + id;
			  table += "<td width=\"300\" height=\"150\" align=center ><img src=\"images/"+ id 
			  			+".jpg\" width=\"80\" height=\"120\" /><br/><a href=\"" +  url + "\" >"+ name 
			  			+"</a></td>";
			  if(i%4 == 3) table += "</tr>";
			  Images = rs.getBinaryStream("Images");
			  showImage(path, id);
			  i++;
		  }
		  rs.close();
	  } catch (SQLException e) {
		  e.printStackTrace();
	  }
	  table += "</table>";
	  return table;
  }
  public String getSortjr(String path) {
	  String table = "<br/><table width=\"800\" border=\"0\" align=\"center\">";
	  dbConnect db = new dbConnect();
	  ResultSet rs = db.executeQuery("select id,name,Images from book where Commend=3");
	  int i = 0;
	  try {
		while(rs.next()){
			  if(i%4 == 0) table += "<tr>";
			  int id = rs.getInt("id");
			  String name = rs.getString("name");
			  String url = "book/information.jsp?bid=" + id;
			  table += "<td width=\"300\" height=\"150\" align=center ><img src=\"images/"+ id 
			  			+".jpg\" width=\"80\" height=\"120\" /><br/><a href=\"" +  url + "\" >"+ name 
			  			+"</a></td>";
			  if(i%4 == 3) table += "</tr>";
			  Images = rs.getBinaryStream("Images");
			  showImage(path, id);
			  i++;
		  }
		  rs.close();
	  } catch (SQLException e) {
		  e.printStackTrace();
	  }
	  table += "</table>";
	  return table;
  }
  public String getSortjj(String path) {
	  String table = "<br/><table width=\"800\" border=\"0\" align=\"center\">";
	  dbConnect db = new dbConnect();
	  ResultSet rs = db.executeQuery("select id,name,Images from book where Commend=4");
	  int i = 0;
	  try {
		while(rs.next()){
			  if(i%4 == 0) table += "<tr>";
			  int id = rs.getInt("id");
			  String name = rs.getString("name");
			  String url = "book/information.jsp?bid=" + id;
			  table += "<td width=\"300\" height=\"150\" align=center ><img src=\"images/"+ id 
			  			+".jpg\" width=\"80\" height=\"120\" /><br/><a href=\"" +  url + "\" >"+ name 
			  			+"</a></td>";
			  if(i%4 == 3) table += "</tr>";
			  Images = rs.getBinaryStream("Images");
			  showImage(path, id);
			  i++;
		  }
		  rs.close();
	  } catch (SQLException e) {
		  e.printStackTrace();
	  }
	  table += "</table>";
	  return table;
  }
  public String getBookList(int act){/*act： 0全部, 1无图, 2有图*/

	  String lst = "";
	  String sql = "select name,press,Commend,id from book";
	  if(act == 1)
		  sql += " where Images is null";
	  else if(act == 2)
		  sql += " where Images is not null";
	  
	  dbConnect db = new dbConnect();
	  ResultSet rs = db.executeQuery(sql);
	  try {
			while(rs.next()){
				lst += "<tr align=\"center\">\n";
				lst += "<td>"+ rs.getString(1) +"</td>\n";
				lst += "<td>"+ rs.getString(2) +"</td>\n";
				lst += "<td>"+ (rs.getInt(3)==1?"精品":"") +"</td>\n";
				lst += "<td><a href=\"UploadImg.jsp?bid="+ rs.getInt(4)
						+"\">[添加图片]</a></td>\n";
			}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	  return lst;
	  
  }
  
  public String getBookList(String key){

	  String lst = "";
	  String sql = "select name,press,Commend,price,repertory,id,intro from book where name like '%"
		  			+ key + "%'";	  
	  
	  dbConnect db = new dbConnect();
	  ResultSet rs = db.executeQuery(sql);
	  try {
			while(rs.next()){
				int bid = rs.getInt(6);
				lst += "<tr align=\"left\">\n";
				lst += "<td>"+ rs.getString(1) +"</td>\n";
				lst += "<td>"+ rs.getString(2) +"</td>\n";
				lst += "<td>"+ rs.getString(5) +"</td>\n";
				lst += "<td>"+ (rs.getInt(3)==1?"精品":"") +"</td>\n";
				lst += "<td>"+ rs.getString(7) +"</td>\n";
				lst += "<td><a href=\"information.jsp?bid="+ 
						bid +"\">[详细资料]</a>\n";
				lst += "<td><a href=# onclick=\"openwin('"+ bid +"');\">放入购物车</a></td>\n";
			}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	  return lst;
	  
  }

public void setImg(String img) {
	this.img = img;
}

public String getImg() {
	return img;
}

public String getCheck() {
	return check;
}

public void setCheck(String check) {
	this.check = check;
}
}
